FROM golang:1.18-alpine AS builder
LABEL stage=builder

RUN mkdir app
WORKDIR /app
RUN apk update && apk upgrade && \
    apk add --update --no-cache \
    gcc g++
COPY ./go.mod ./go.sum ./
RUN go mod download
COPY . .
RUN go build -o main .
RUN ls


FROM redis:alpine
RUN apk add --no-cache tzdata
RUN mkdir app
WORKDIR /app
COPY --from=builder /app/main .

