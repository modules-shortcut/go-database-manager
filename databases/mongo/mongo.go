package mongo

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/modules-shortcut/go-utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"reflect"
	"regexp"
	"strings"
	"time"
)

var matchFirstCap = regexp.MustCompile("(.)([A-Z][a-z]+)")
var matchAllCap = regexp.MustCompile("([a-z0-9])([A-Z])")

//Model of Collection
type Model struct {
	ID        primitive.ObjectID `bson:"_id" json:"id" form:"id"`
	CreatedAt int64              `bson:"created_at" json:"created_at" form:"created_at"`
	UpdatedAt int64              `bson:"updated_at" json:"updated_at" form:"updated_at"`

	//Pagination and Filter
	Pagination Pagination `json:"-" form:"-" bson:"-"`
	//Practice
	IsTest bool `json:"-" form:"tutorial" bson:"-"`
}
type Pagination struct {
	//Search
	Keyword   string `json:"-" form:"keyword"`
	StartDate int64  `json:"-" form:"start_date"`
	EndDate   int64  `json:"-" form:"end_date"`
	Sort      string `json:"-" form:"sort"`
	SortBy    string `json:"-" form:"sort_by"`

	//Pagination
	ShowAll   bool  `json:"-" form:"show_all"`
	Page      int64 `json:"current_page" form:"page"`
	PageSize  int64 `json:"page_size" form:"page_size"`
	TotalPage int64 `json:"page_total"`
	TotalData int64 `json:"data_total"`
	HasNext   bool  `json:"has_next"`
	HasPrev   bool  `json:"has_previous"`
}

func (m *Model) Bind(ctx *gin.Context, data interface{}) (err error) {
	//Bind Query Url
	if err = ctx.ShouldBindQuery(m); err != nil {
		utils.ErrorHandler(err)
		return
	}
	//get pagination data
	if err = ctx.ShouldBindQuery(&m.Pagination); err != nil {
		utils.ErrorHandler(err)
		return
	}
	//Bind Header
	if err = ctx.ShouldBindHeader(data); err != nil {
		utils.ErrorHandler(err)
		return
	}
	err = ctx.ShouldBind(data)
	utils.ErrorHandler(err)
	return
}

func toSnakeCase(str string) string {
	snake := matchFirstCap.ReplaceAllString(str, "${1}_${2}")
	snake = matchAllCap.ReplaceAllString(snake, "${1}_${2}")
	return strings.ToLower(snake)
}
func toBsonM(data interface{}, res interface{}) (err error) {
	var bsonBytes []byte

	if bsonBytes, err = bson.Marshal(data); err != nil {
		utils.ErrorHandler(err)
		return
	}

	return bson.Unmarshal(bsonBytes, &res)
}
func toStruct(data bson.M, result interface{}) (err error) {
	var bsonBytes []byte

	if bsonBytes, err = bson.Marshal(data); err != nil {
		utils.ErrorHandler(err)
		return
	}

	return bson.Unmarshal(bsonBytes, result)
}
func isPtrSlice(i interface{}) bool {
	if i == nil {
		return false
	}
	v := reflect.ValueOf(i)
	if v.Kind() != reflect.Ptr {
		return false
	}
	return v.Elem().Kind() == reflect.Slice
}

//GetNameOf is a func for get name of structor model and make if snake case
func (this *Model) getNameOf(type_ interface{}, prefix string) string {
	names := strings.Split(reflect.TypeOf(type_).String(), ".")
	if length := len(names); length < 1 {
		return toSnakeCase(prefix + "unknown")
	} else {
		if length > 1 {
			return toSnakeCase(prefix + names[len(names)-1] + "s")
		}
	}
	return toSnakeCase(prefix + names[0] + "s")
}
func (this *Model) getCollection(Db *mongo.Database, mdl string, opts ...*options.CollectionOptions) (coll *mongo.Collection) {
	if this.IsTest {
		mdl = mdl + "_test"
	}

	return Db.Collection(mdl, opts...)
}

func (this *Model) Create(mdl interface{}) (err error) {
	var (
		mapTemp map[string]interface{}
		id      *mongo.InsertOneResult
		Db      *mongo.Database
		temp    = make(bson.M)
		client  *mongo.Client
	)

	if Db, client, err = db.Connect(); err != nil {
		utils.ErrorHandler(err)
		return
	}
	defer func(client *mongo.Client, ctx context.Context) {
		_ = client.Disconnect(ctx)
	}(client, context.TODO())

	if err = toBsonM(mdl, &temp); err != nil {
		utils.ErrorHandler(err)
		return
	}

	if key, val := temp["_id"]; !val && key == nil {
		temp["_id"] = primitive.NewObjectID()
	}

	if key, val := temp["created_at"]; !val || key == nil {
		temp["created_at"] = time.Now().Local().Unix()
	}

	if key, val := temp["updated_at"]; !val || key == nil {
		temp["updated_at"] = time.Now().Local().Unix()
	}

	if id, err = this.getCollection(Db, this.getNameOf(mdl, "")).InsertOne(context.TODO(), temp); err != nil {
		utils.ErrorHandler(err)
		return
	}

	temp["_id"] = id.InsertedID
	if err = toStruct(temp, mdl); err != nil {
		utils.ErrorHandler(err)
		return
	}

	//Convert to map[string]interface{}
	if err = utils.Convert(mdl, &mapTemp); err != nil {
		utils.ErrorHandler(err)
		return
	}
	mapTemp["id"] = id.InsertedID
	mapTemp["created_at"] = temp["created_at"]
	mapTemp["updated_at"] = temp["updated_at"]

	//Convert to map[string]interface{}
	if err = utils.Convert(mapTemp, mdl); err != nil {
		utils.ErrorHandler(err)
		return
	}

	return
}

func (this *Model) Get(data interface{}, filter bson.M, opts ...*options.FindOptions) (err error) {
	var (
		opt    = options.Find()
		Db     *mongo.Database
		client *mongo.Client
		cur    *mongo.Cursor
		res    []map[string]interface{}
	)

	if Db, client, err = db.Connect(); err != nil {
		utils.ErrorHandler(err)
		return
	}
	defer func(client *mongo.Client, ctx context.Context) {
		_ = client.Disconnect(ctx)
	}(client, context.TODO())

	//if !isPtrSlice(data) {
	//	err = m.conn.Take(data, args...).Error
	//} else {
	//	err = m.Pagination.init(m.conn, data, &m.ILike, args...).Error
	//}

	//Set default value
	if this.Pagination.Page < 1 {
		this.Pagination.Page = 1
	}
	if this.Pagination.PageSize < 1 {
		this.Pagination.PageSize = 10
	}
	if this.Pagination.SortBy == "" {
		this.Pagination.SortBy = "_id"
	}
	if this.Pagination.Sort == "" {
		this.Pagination.Sort = "ASC"
	}
	this.Pagination.Sort = strings.ReplaceAll(this.Pagination.Sort, " ", "")

	if this.Pagination.StartDate > 0 && this.Pagination.EndDate > 0 {
		filter["created_at"] = bson.M{
			"$gte": this.Pagination.StartDate,
			"$lte": this.Pagination.EndDate,
		}
	}
	if !this.ID.IsZero() {
		filter["_id"] = this.ID
	}

	if this.Pagination.TotalData, err = this.getCollection(Db, this.getNameOf(data, "")).CountDocuments(context.TODO(), filter); err != nil {
		utils.ErrorHandler(err)
		return
	}
	this.Pagination.TotalPage = (this.Pagination.TotalData + this.Pagination.PageSize - 1) / this.Pagination.PageSize
	this.Pagination.HasNext = utils.Gte(this.Pagination.Page, this.Pagination.TotalPage, false, true).(bool)
	this.Pagination.HasPrev = utils.Lt(this.Pagination.Page, int64(2), false, true).(bool)

	//Order
	opt.SetSort(bson.D{{this.Pagination.SortBy, utils.Eq(strings.ToLower(this.Pagination.Sort), "asc", 1, -1)}})
	// Pagination
	opt.SetSkip((this.Pagination.Page - 1) * this.Pagination.PageSize)
	opt.SetLimit(this.Pagination.PageSize)
	opts = append(opts, opt)

	if cur, err = this.getCollection(Db, this.getNameOf(data, "")).Find(context.TODO(), filter, opts...); err != nil {
		utils.ErrorHandler(err)
		return
	}
	defer cur.Close(context.TODO())

	// Iterate over the results
	for cur.Next(context.TODO()) {
		var (
			result primitive.M
			temp   map[string]interface{}
		)
		if err = cur.Decode(&result); err != nil {
			utils.ErrorHandler(err)
			return
		}

		// Process the document here
		if err = utils.Convert(result, &temp); err != nil {
			utils.ErrorHandler(err)
			return
		}
		temp["id"] = result["_id"]
		temp["created_at"] = result["created_at"]
		temp["updated_at"] = result["updated_at"]
		res = append(res, temp)
	}

	// Check for errors during cursor iteration
	if err = cur.Err(); err != nil {
		utils.ErrorHandler(err)
		return
	}

	if !isPtrSlice(data) {
		if len(res) > 0 {
			return utils.Convert(res[0], data)
		}
		return
	}

	return utils.Convert(res, data)
}

func (this *Model) Update(data interface{}, filter bson.M, opts ...*options.UpdateOptions) (err error) {

	var (
		now    = time.Now().Local().Unix()
		Db     *mongo.Database
		client *mongo.Client
		temps  []map[string]interface{}
		temp   map[string]interface{}
	)

	if Db, client, err = db.Connect(); err != nil {
		utils.ErrorHandler(err)
		return
	}
	defer func(client *mongo.Client, ctx context.Context) {
		_ = client.Disconnect(ctx)
	}(client, context.TODO())

	switch isPtrSlice(data) {
	case true:
		if err = utils.Convert(data, &temps); err != nil {
			utils.ErrorHandler(err)
			return
		}

		for idx, _ := range temps {
			temps[idx]["updated_at"] = now
		}

		if _, err = this.getCollection(Db, this.getNameOf(data, "")).UpdateMany(context.TODO(), filter, bson.D{{Key: "$set", Value: temps}}, opts...); err != nil {
			utils.ErrorHandler(err)
			return
		}

	default:

		if err = utils.Convert(data, &temp); err != nil {
			utils.ErrorHandler(err)
			return
		}
		if !this.ID.IsZero() {
			filter["_id"] = this.ID
		}
		temp["updated_at"] = now
		if _, err = this.getCollection(Db, this.getNameOf(data, "")).UpdateOne(context.TODO(), filter, bson.D{{Key: "$set", Value: temp}}, opts...); err != nil {
			utils.ErrorHandler(err)
			return
		}
	}

	return
}

func (this *Model) Delete(data interface{}, filter bson.M, opts ...*options.DeleteOptions) (err error) {

	var (
		Db     *mongo.Database
		client *mongo.Client
	)

	if Db, client, err = db.Connect(); err != nil {
		utils.ErrorHandler(err)
		return
	}
	defer func(client *mongo.Client, ctx context.Context) {
		_ = client.Disconnect(ctx)
	}(client, context.TODO())

	if !this.ID.IsZero() {
		filter["_id"] = this.ID
	}

	if _, err = this.getCollection(Db, this.getNameOf(data, "")).DeleteMany(context.TODO(), filter, opts...); err != nil {
		utils.ErrorHandler(err)
		return
	}

	return
}
