package mongo

import (
	"context"
	"fmt"
	"gitlab.com/modules-shortcut/go-database-manager/databases"
	"gitlab.com/modules-shortcut/go-utils"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"os"
	"time"
)

var db Mongo

// Connection
type Mongo struct {
	Config databases.Config
	//Db     *mongo.Database
	//Client *mongo.Client
}

func (mdb *Mongo) Connect() (Db *mongo.Database, client *mongo.Client, err error) {
	var (
		cancel context.CancelFunc
		ctx    context.Context
	)

	if client, err = mongo.NewClient(options.Client().
		ApplyURI(fmt.Sprintf("mongodb://%s:%s", mdb.Config.HOST, mdb.Config.PORT)).
		SetAuth(options.Credential{
			AuthSource: mdb.Config.NAME,
			Username:   mdb.Config.USER,
			Password:   mdb.Config.PASS,
		})); err != nil {
		utils.ErrorHandler(err)
		panic(err)
	}

	ctx, cancel = context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err = client.Connect(ctx); err != nil {
		utils.ErrorHandler(err)
		panic(err)
	}
	Db = client.Database(mdb.Config.NAME)

	return
}

//func (mdb *Mongo) Collection(col string) (collection *mongo.Collection) {
//	return mdb.Db.Collection(col)
//}

func init() {

	if name := os.Getenv("ENGINE_NAME"); name == "" {
		utils.ErrorHandler(os.Setenv("ENGINE_NAME", "ENGINE"))
	}

	if err := db.Config.Parse(); err != nil {
		utils.ErrorHandler(err)
		panic(err)
	}

	//if err := db.Connect(); err != nil {
	//	utils.ErrorHandler(err)
	//	panic(err)
	//}

}

//func Collection(col string, isTest bool) (client *mongo.Collection) {
//	if isTest {
//		col = col + "_test"
//	}
//	return db.Collection(col)
//}
