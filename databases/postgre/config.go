package postgre

import (
	"database/sql"
	"gitlab.com/modules-shortcut/go-database-manager/databases"
	"gitlab.com/modules-shortcut/go-utils"
	"gorm.io/gorm"
	"os"
)

type PGSql struct {
	cfg *gorm.Config
}

var (
	database     databases.PostgreSQL
	databaseName string
)

func init() {
	if name := os.Getenv("ENGINE_NAME"); name == "" {
		utils.ErrorHandler(os.Setenv("ENGINE_NAME", "ENGINE"))
	}

	if err := database.Config.Parse(); err != nil {
		utils.ErrorHandler(err)
		panic(err)
	}

	databaseName = database.Config.NAME
}

func DB(isTest bool, cfg *gorm.Config) (DB *gorm.DB, sql_ *sql.DB) {

	if isTest {
		database.Config.NAME = databaseName + "_test"
	} else {
		database.Config.NAME = databaseName
	}

	return database.Connect(cfg)
}

func Conn(cfg *gorm.Config) (db *gorm.DB) {
	db, _ = DB(false, cfg)
	return
}

func TestConn(cfg *gorm.Config) (db *gorm.DB) {
	db, _ = DB(true, cfg)
	return
}
