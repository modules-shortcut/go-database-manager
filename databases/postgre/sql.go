package postgre

import (
	"database/sql"
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/modules-shortcut/go-database-manager/databases"
	"gitlab.com/modules-shortcut/go-utils"
	"gorm.io/gorm"
	"math"
	"reflect"
	"regexp"
	"strings"
	"time"
)

var matchFirstCap = regexp.MustCompile("(.)([A-Z][a-z]+)")
var matchAllCap = regexp.MustCompile("([a-z0-9])([A-Z])")

type Pagination struct {
	//Search
	Keyword   string `json:"-" form:"keyword"`
	StartDate int64  `json:"-" form:"start_date"`
	EndDate   int64  `json:"-" form:"end_date"`
	Sort      string `json:"-" form:"sort"`
	SortBy    string `json:"-" form:"sort_by"`

	//Pagination
	ShowAll   bool  `json:"-" form:"show_all"`
	Page      int64 `json:"current_page" form:"page"`
	PageSize  int64 `json:"page_size" form:"page_size"`
	TotalPage int64 `json:"page_total"`
	TotalData int64 `json:"data_total"`
	HasNext   bool  `json:"has_next"`
	HasPrev   bool  `json:"has_previous"`
}

func (p *Pagination) init(db *gorm.DB, data interface{}, ilike *[]string, args ...interface{}) *gorm.DB {
	if p.Page < 1 {
		p.Page = 1
	}
	if p.PageSize < 1 {
		p.PageSize = 10
	}
	if p.SortBy == "" {
		p.SortBy = "id"
	}
	if p.Sort == "" {
		p.Sort = "DESC"
	}
	p.Sort = strings.ReplaceAll(p.Sort, " ", "")

	//if show all data
	if p.ShowAll {
		return db.Order(p.SortBy+" "+p.Sort).Find(data, args...).Count(&p.TotalData)
	}

	// get total data
	var queries []string
	if ilike != nil {
		for _, elm := range *ilike {
			queries = append(queries, fmt.Sprintf("%s ILIKE '%s'", elm, "%"+p.Keyword+"%"))
		}
	}
	var query = strings.Join(queries, " OR ")
	if p.StartDate > 0 && p.EndDate > 0 {
		var (
			tempQuery string
			arg       []interface{}
		)
		if len(args) > 0 {
			tempQuery = args[0].(string)
			if len(args) == 1 {
				arg = nil
			} else {
				arg = args[1:]
			}
		}

		utils.ErrorHandler(db.Where("created_at BETWEEN ? AND ?", p.StartDate, p.EndDate).Where(query).Where(tempQuery, arg...).Count(&p.TotalData).Error)
	} else {
		var (
			tempQuery string
			arg       []interface{}
		)
		if len(args) > 0 {
			tempQuery = args[0].(string)
			if len(args) == 1 {
				arg = nil
			} else {
				arg = args[1:]
			}
		}
		utils.ErrorHandler(db.Where(query).Where(tempQuery, arg...).Count(&p.TotalData).Error)
	}

	p.TotalPage = int64(math.Ceil(float64(p.TotalData) / float64(p.PageSize)))

	p.HasNext = true
	p.HasPrev = true

	if p.Page < 2 {
		p.HasPrev = false
	}

	if p.Page >= p.TotalPage {
		p.HasNext = false
	}
	if p.StartDate > 0 && p.EndDate > 0 {
		return db.Offset(int((p.Page-1)*p.PageSize)).Limit(int(p.PageSize)).Where("created_at BETWEEN ? AND ?", p.StartDate, p.EndDate).Where(query).Order(p.SortBy+" "+p.Sort).Find(data, args...)
	}
	return db.Offset(int((p.Page-1)*p.PageSize)).Limit(int(p.PageSize)).Where(query).Order(p.SortBy+" "+p.Sort).Find(data, args...)
}

type PaginationDT struct {
	//Search
	Keyword   string `json:"-" form:"keyword"`
	StartDate string `json:"-" form:"start_date"`
	EndDate   string `json:"-" form:"end_date"`
	Sort      string `json:"-" form:"sort"`
	SortBy    string `json:"-" form:"sort_by"`

	//Pagination
	ShowAll   bool  `json:"-" form:"show_all"`
	Page      int64 `json:"current_page" form:"page"`
	PageSize  int64 `json:"page_size" form:"page_size"`
	TotalPage int64 `json:"page_total"`
	TotalData int64 `json:"data_total"`
	HasNext   bool  `json:"has_next"`
	HasPrev   bool  `json:"has_previous"`
}

func (p *PaginationDT) init(db *gorm.DB, data interface{}, ilike *[]string, args ...interface{}) *gorm.DB {
	if p.Page < 1 {
		p.Page = 1
	}
	if p.PageSize < 1 {
		p.PageSize = 10
	}
	if p.SortBy == "" {
		p.SortBy = "id"
	}
	if p.Sort == "" {
		p.Sort = "DESC"
	}
	p.Sort = strings.ReplaceAll(p.Sort, " ", "")

	//if show all data
	if p.ShowAll {
		return db.Order(p.SortBy+" "+p.Sort).Find(data, args...).Count(&p.TotalData)
	}

	// get total data
	var queries []string
	if ilike != nil {
		for _, elm := range *ilike {
			queries = append(queries, fmt.Sprintf("%s ILIKE '%s'", elm, "%"+p.Keyword+"%"))
		}
	}
	var query = strings.Join(queries, " OR ")
	if p.StartDate != "" && p.EndDate != "" {
		var (
			tempQuery string
			arg       []interface{}
		)
		if len(args) > 0 {
			tempQuery = args[0].(string)
			if len(args) == 1 {
				arg = nil
			} else {
				arg = args[1:]
			}
		}
		utils.ErrorHandler(db.Model(data).Where("created_at BETWEEN ? AND ?", p.StartDate+" 00:00:00", p.EndDate+" 23:59:59").Where(query).Where(tempQuery, arg...).Count(&p.TotalData).Error)
	} else {
		var (
			tempQuery string
			arg       []interface{}
		)
		if len(args) > 0 {
			tempQuery = args[0].(string)
			if len(args) == 1 {
				arg = nil
			} else {
				arg = args[1:]
			}
		}
		utils.ErrorHandler(db.Model(data).Where(query).Where(tempQuery, arg...).Count(&p.TotalData).Error)
	}

	p.TotalPage = int64(math.Ceil(float64(p.TotalData) / float64(p.PageSize)))

	p.HasNext = true
	p.HasPrev = true

	if p.Page < 2 {
		p.HasPrev = false
	}

	if p.Page >= p.TotalPage {
		p.HasNext = false
	}
	if p.StartDate != "" && p.EndDate != "" {
		return db.Offset(int((p.Page-1)*p.PageSize)).Limit(int(p.PageSize)).Where("created_at BETWEEN ? AND ?", p.StartDate+" 00:00:00", p.EndDate+" 23:59:59").Where(query).Order(p.SortBy+" "+p.Sort).Find(data, args...)
	}

	return db.Offset(int((p.Page-1)*p.PageSize)).Limit(int(p.PageSize)).Where(query).Order(p.SortBy+" "+p.Sort).Find(data, args...)
}

type Model struct {
	ID        uint  `gorm:"primarykey;index" json:"id" form:"id"`
	CreatedAt int64 `gorm:"index" json:"created_at" form:"created_at"`
	UpdatedAt int64 `gorm:"index" json:"updated_at" form:"updated_at"`

	//Pagination and Filter
	Pagination Pagination `json:"-" form:"-" gorm:"-"`
	//Practice
	IsTest bool `json:"-" form:"tutorial" gorm:"-"`

	//ILike cond
	ILike   []string `json:"-" gorm:"-"`
	Preload []string `json:"-" gorm:"-"`

	//Connection
	conn       *gorm.DB `json:"-" form:"-" gorm:"-"`
	sql        *sql.DB  `json:"-" form:"-" gorm:"-"`
	customConn bool     `json:"-" form:"-" gorm:"-"`
}

//open for open connection to database
func (m *Model) open() {
	m.conn, m.sql = DB(m.IsTest, nil)
}

//close for close connection from database
// don't forget to using defer to exec this func at the end of your function witch open connection to database
func (m *Model) close() {
	utils.ErrorHandler(m.sql.Close())
}

//Create func for create data to table base on model in data params
func (m *Model) Create(data interface{}) (err error) {

	//Validate
	if err = databases.RequiredCheck("validate", data); err != nil {
		return
	}

	if !m.customConn {
		m.open()        //to open connection to database
		defer m.close() // to close connection from database after this function finish
	}

	return m.conn.Create(data).Error
}

//Update func for update data from table base on model in data params and update by args
func (m *Model) Update(data interface{}, args ...interface{}) (err error) {
	if !m.customConn {
		m.open()        //to open connection to database
		defer m.close() // to close connection from database after this function finish
	}

	//update where args is nil means update by id
	if len(args) < 1 {
		return m.conn.Where("id = ?", m.ID).Updates(data).Error
	}
	return m.conn.Where(args[0], args[1:]...).Updates(data).Error
}

//Delete func for delete data from table base on model in data params and delete by args
func (m *Model) Delete(data interface{}, args ...interface{}) (err error) {
	if !m.customConn {
		m.open()        //to open connection to database
		defer m.close() // to close connection from database after this function finish
	}

	if len(args) < 1 {
		return m.conn.Delete(data, "id = ?", m.ID).Error
	}
	return m.conn.Delete(data, args...).Error
}

//Read func for get data
func (m *Model) Read(data interface{}, args ...interface{}) (err error) {

	if !m.customConn {
		m.open()        //to open connection to database
		defer m.close() // to close connection from database after this function finish
	}

	if data == nil {
		return errors.New("data can't be nil")
	}

	//if have preload statement
	for _, elm := range m.Preload {
		m.conn = m.conn.Preload(elm)
	}

	//get by id
	if m.ID > 0 {
		if len(args) > 0 {
			args = append(args, " AND id = ? ")
		} else {
			args = append(args, " id = ? ")
		}
		args = append(args, m.ID)
	}

	if !isPtrSlice(data) {
		err = m.conn.Take(data, args...).Error
	} else {
		err = m.Pagination.init(m.conn, data, &m.ILike, args...).Error
	}

	if err != nil {
		utils.LogPrint(fmt.Sprintf("from model %v", reflect.TypeOf(data)))
		utils.ErrorHandler(err)
	}

	return
}

func (m *Model) SetConnection(conn *gorm.DB) *Model {
	m.customConn = true
	m.conn = conn
	return m
}

//SetPreload func for add some preload relation to model
func (m *Model) SetPreload(pre ...string) *Model {
	m.Preload = pre
	return m
}

//SetILike for add some ilike condition
func (m *Model) SetILike(ilike ...string) *Model {
	m.ILike = ilike
	return m
}

func (m *Model) Bind(ctx *gin.Context, data interface{}) (err error) {
	//Bind Query Url
	if err = ctx.ShouldBindQuery(m); err != nil {
		utils.ErrorHandler(err)
		return
	}
	//get pagination data
	if err = ctx.ShouldBindQuery(&m.Pagination); err != nil {
		utils.ErrorHandler(err)
		return
	}
	//Bind Header
	if err = ctx.ShouldBindHeader(data); err != nil {
		utils.ErrorHandler(err)
		return
	}
	err = ctx.ShouldBind(data)
	utils.ErrorHandler(err)
	return
}

func isPtrSlice(i interface{}) bool {
	if i == nil {
		return false
	}
	v := reflect.ValueOf(i)
	if v.Kind() != reflect.Ptr {
		return false
	}
	return v.Elem().Kind() == reflect.Slice
}

//GetNameOf is a func for get name of structor model and make if snake case
func (this *Model) GetNameOf(type_ interface{}, prefix string) string {
	names := strings.Split(reflect.TypeOf(type_).String(), ".")
	if length := len(names); length < 1 {
		return toSnakeCase(prefix + "unknown")
	} else {
		if length > 1 {
			return toSnakeCase(prefix + names[len(names)-1] + "s")
		}
	}
	return toSnakeCase(prefix + names[0] + "s")
}

func toSnakeCase(str string) string {
	snake := matchFirstCap.ReplaceAllString(str, "${1}_${2}")
	snake = matchAllCap.ReplaceAllString(snake, "${1}_${2}")
	return strings.ToLower(snake)
}

type ModelDT struct {
	ID        uint      `gorm:"primarykey;index" json:"id" form:"id"`
	CreatedAt time.Time `gorm:"index" json:"created_at" form:"created_at"`
	UpdatedAt time.Time `gorm:"index" json:"updated_at" form:"updated_at"`

	//Pagination and Filter
	Pagination PaginationDT `json:"-" form:"-" gorm:"-"`
	//Practice
	IsTest bool `json:"-" form:"tutorial" gorm:"-"`

	//ILike cond
	ILike   []string `json:"-" gorm:"-"`
	Preload []string `json:"-" gorm:"-"`

	//Connection
	conn       *gorm.DB `json:"-" form:"-" gorm:"-"`
	sql        *sql.DB  `json:"-" form:"-" gorm:"-"`
	customConn bool     `json:"-" form:"-" gorm:"-"`
}

//open for open connection to database
func (m *ModelDT) open() {
	m.conn, m.sql = DB(m.IsTest, nil)
}

//close for close connection from database
// don't forget to using defer to exec this func at the end of your function witch open connection to database
func (m *ModelDT) close() {
	utils.ErrorHandler(m.sql.Close())
}

//Create func for create data to table base on model in data params
func (m *ModelDT) Create(data interface{}) (err error) {

	//Validate
	if err = databases.RequiredCheck("validate", data); err != nil {
		return
	}

	if !m.customConn {
		m.open()        //to open connection to database
		defer m.close() // to close connection from database after this function finish
	}

	return m.conn.Create(data).Error
}

//Update func for update data from table base on model in data params and update by args
func (m *ModelDT) Update(data interface{}, args ...interface{}) (err error) {
	if !m.customConn {
		m.open()        //to open connection to database
		defer m.close() // to close connection from database after this function finish
	}

	//update where args is nil means update by id
	if len(args) < 1 {
		return m.conn.Where("id = ?", m.ID).Updates(data).Error
	}
	return m.conn.Where(args[0], args[1:]...).Updates(data).Error
}

//Delete func for delete data from table base on model in data params and delete by args
func (m *ModelDT) Delete(data interface{}, args ...interface{}) (err error) {
	if !m.customConn {
		m.open()        //to open connection to database
		defer m.close() // to close connection from database after this function finish
	}

	if len(args) < 1 {
		return m.conn.Delete(data, "id = ?", m.ID).Error
	}
	return m.conn.Delete(data, args...).Error
}

//Read func for get data
func (m *ModelDT) Read(data interface{}, args ...interface{}) (err error) {

	if !m.customConn {
		m.open()        //to open connection to database
		defer m.close() // to close connection from database after this function finish
	}

	if data == nil {
		return errors.New("data can't be nil")
	}

	//if have preload statement
	for _, elm := range m.Preload {
		m.conn = m.conn.Preload(elm)
	}

	//get by id
	if m.ID > 0 {
		if len(args) > 0 {
			args = append(args, " AND id = ? ")
		} else {
			args = append(args, " id = ? ")
		}
		args = append(args, m.ID)
	}

	if !isPtrSlice(data) {
		err = m.conn.Take(data, args...).Error
	} else {
		err = m.Pagination.init(m.conn, data, &m.ILike, args...).Error
	}

	if err != nil {
		utils.LogPrint(fmt.Sprintf("from model %v", reflect.TypeOf(data)))
		utils.ErrorHandler(err)
	}

	return
}

func (m *ModelDT) SetConnection(conn *gorm.DB) *ModelDT {
	m.customConn = true
	m.conn = conn
	return m
}

//SetPreload func for add some preload relation to model
func (m *ModelDT) SetPreload(pre ...string) *ModelDT {
	m.Preload = pre
	return m
}

//SetILike for add some ilike condition
func (m *ModelDT) SetILike(ilike ...string) *ModelDT {
	m.ILike = ilike
	return m
}

func (m *ModelDT) Bind(ctx *gin.Context, data interface{}) (err error) {
	//Bind Query Url
	if err = ctx.ShouldBindQuery(m); err != nil {
		utils.ErrorHandler(err)
		return
	}
	//get pagination data
	if err = ctx.ShouldBindQuery(&m.Pagination); err != nil {
		utils.ErrorHandler(err)
		return
	}
	//Bind Header
	if err = ctx.ShouldBindHeader(data); err != nil {
		utils.ErrorHandler(err)
		return
	}
	err = ctx.ShouldBind(data)
	utils.ErrorHandler(err)
	return
}

//GetNameOf is a func for get name of structor model and make if snake case
func (this *ModelDT) GetNameOf(type_ interface{}, prefix string) string {
	names := strings.Split(reflect.TypeOf(type_).String(), ".")
	if length := len(names); length < 1 {
		return toSnakeCase(prefix + "unknown")
	} else {
		if length > 1 {
			return toSnakeCase(prefix + names[len(names)-1] + "s")
		}
	}
	return toSnakeCase(prefix + names[0] + "s")
}
