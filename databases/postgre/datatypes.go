package postgre

import (
	"database/sql/driver"
	"errors"
	"fmt"
	"time"
)

type DateTime int64

// Scan implements sql.Scanner interface and scans value into Time,
func (dt *DateTime) Scan(src interface{}) error {
	switch v := src.(type) {
	case time.Time:
		dt.parseFromTime(v)
	case int64:
		dt.parseFromInt64(v)
	default:
		return errors.New(fmt.Sprintf("failed to scan value: %v", v))
	}

	return nil
}

func (dt *DateTime) parseFromTime(v time.Time) {
	*dt = DateTime(v.Unix())
}

func (dt *DateTime) parseFromInt64(v int64) {
	*dt = DateTime(v)
}

func (dt DateTime) Value() (driver.Value, error) {
	return time.Unix(int64(dt), 0), nil
}
