package cockroach

import (
	"database/sql"
	"gitlab.com/modules-shortcut/go-database-manager/databases"
	"gitlab.com/modules-shortcut/go-utils"
	"gorm.io/gorm"
	"os"
)

var (
	database     databases.CockroachDB
	databaseName string
)

func init() {
	if name := os.Getenv("ENGINE_NAME"); name == "" {
		utils.ErrorHandler(os.Setenv("ENGINE_NAME", "ENGINE"))
	}

	if err := database.Config.Parse(); err != nil {
		utils.ErrorHandler(err)
		panic(err)
	}

	databaseName = database.Config.NAME
}

func DB(isTest ...bool) (DB *gorm.DB, sql_ *sql.DB) {

	if len(isTest) > 0 && isTest[0] {
		database.Config.NAME = databaseName + "_test"
	} else {
		database.Config.NAME = databaseName
	}

	return database.Connect(nil)
}
