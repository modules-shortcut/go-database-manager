package databases

import (
	"errors"
	"fmt"
	"reflect"
	"strings"
)

/*
	THIS SECTION FOR UTILS
*/

//validate is a func for validate tag from model
func RequiredCheck(t string, data interface{}) (err error) {

	var (
		m    = reflect.TypeOf(data)
		v    = reflect.ValueOf(data)
		errs []string
	)

	if m.Kind() == reflect.Ptr {
		m = m.Elem()
	}

	if v.Kind() == reflect.Ptr {
		v = v.Elem()
	}

	for i := 0; i < m.NumField(); i++ {
		var (
			field = m.Field(i)
			tag   = field.Tag.Get(t)
			val   any
		)

		if !field.IsExported() {
			continue
		}
		val = v.Field(i).Interface()

		if tag == "true" {
			switch field.Type.Name() {
			case "invalid":
				errs = append(errs, "invalid type")
			case "bool":
				//TODO: add condition
				if fv, ok := val.(bool); !ok || !fv {
					errs = append(errs, fmt.Sprintf("field %s is requied", field.Name))
				}
			case "int":
			case "int8":
			case "int16":
			case "int32":
			case "int64":
			case "uint":
				if fv, ok := val.(uint); !ok || fv < 1 {
					errs = append(errs, fmt.Sprintf("field %s is requied", field.Name))
				}
			case "uint8":
				if fv, ok := val.(uint8); !ok || fv < 1 {
					errs = append(errs, fmt.Sprintf("field %s is requied", field.Name))
				}
			case "uint16":
				if fv, ok := val.(uint16); !ok || fv < 1 {
					errs = append(errs, fmt.Sprintf("field %s is requied", field.Name))
				}
			case "uint32":
				if fv, ok := val.(uint32); !ok || fv < 1 {
					errs = append(errs, fmt.Sprintf("field %s is requied", field.Name))
				}
			case "uint64":
				if fv, ok := val.(uint64); !ok || fv < 1 {
					errs = append(errs, fmt.Sprintf("field %s is requied", field.Name))
				}
			case "uintptr":
				if fv, ok := val.(uintptr); !ok || fv < 1 {
					errs = append(errs, fmt.Sprintf("field %s is requied", field.Name))
				}
			case "float32":
				if fv, ok := val.(float32); !ok || fv < 1 {
					errs = append(errs, fmt.Sprintf("field %s is requied", field.Name))
				}
			case "float64":
				if fv, ok := val.(float64); !ok || fv < 1 {
					errs = append(errs, fmt.Sprintf("field %s is requied", field.Name))
				}
			case "complex64":
			case "complex128":
			case "array":
			case "chan":
			case "func":
			case "interface":
			case "map":
			case "ptr":
			case "slice":
			case "string":
				if fv, ok := val.(string); !ok || fv == "" {
					errs = append(errs, fmt.Sprintf("field %s is requied", field.Name))
				}
			case "struct":
			case "unsafe.Pointer":

			}
		}
	}

	if len(errs) > 0 {
		err = errors.New(strings.Join(errs, ", "))
	}

	return
}
