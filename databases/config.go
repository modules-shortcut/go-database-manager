package databases

import "github.com/caarlos0/env/v6"

// Config for databases config
type Config struct {
	//DATABASE
	HOST string `env:"DB_HOST" envDefault:"localhost" json:"db_host"`
	PORT string `env:"DB_PORT" envDefault:"27107" json:"db_port"`
	NAME string `env:"DB_NAME" json:"db_name"`
	USER string `env:"DB_USER" json:"db_user"`
	PASS string `env:"DB_PASS" json:"db_pass"`
	SSL  string `env:"DB_SSL" envDefault:"disable" json:"db_ssl"`
	TZ   string `env:"TIME_ZONE" envDefault:"Asia/Jakarta" json:"db_tz"`

	MAX_IDLE_CONNS      int `env:"MAX_IDLE_CONNS" json:"max_idle_conns"`           //default 100
	MAX_OPEN_CONNS      int `env:"MAX_OPEN_CONNS" json:"max_open_conns"`           //default 1000
	MAX_IDLE_CONNS_TIME int `env:"MAX_IDLE_CONNS_TIME" json:"max_idle_conns_time"` //default 60m

	//ETC
	EngineName string `env:"ENGINE_NAME" json:"engine_name,omitempty"`

	//Additional GORM CONFIG
	SkipDefaultTransaction bool `env:"SkipDefaultTransaction" envDefault:"false" json:"skip_default_transaction"`
}

//Parse for parse env variables to this struct
func (this *Config) Parse() (err error) {
	return env.Parse(this)
}
