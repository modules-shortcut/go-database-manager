package databases

import (
	"database/sql"
	"fmt"
	"github.com/uptrace/uptrace-go/uptrace"
	"gitlab.com/modules-shortcut/go-utils"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/plugin/opentelemetry/tracing"
	"os"
	"strings"
	"time"
)

type PostgreSQL struct {
	Config Config
}

func (pgsql *PostgreSQL) Connect(cfg *gorm.Config) (DB *gorm.DB, sql_ *sql.DB) {

	var (
		dsn string
		err error
	)

	//Set DSN connection
	// dbname for postgres version <= 14 and database for postgres above 14
	dsn = fmt.Sprintf("host=%s user=%s password=%s dbname=%s database=%s port=%s sslmode=%s TimeZone=%s",
		pgsql.Config.HOST,
		pgsql.Config.USER,
		pgsql.Config.PASS,
		pgsql.Config.NAME, //for postgres version <= 14
		pgsql.Config.NAME, //for postgres above 14
		pgsql.Config.PORT,
		pgsql.Config.SSL,
		pgsql.Config.TZ,
	)

	//set gorm config if exist
	if cfg == nil {
		cfg = &gorm.Config{}
	}

	//create connection
	if DB, err = gorm.Open(postgres.Open(dsn), cfg); err != nil {
		utils.ErrorHandler(err)
		panic(err)
	}

	if strings.ToLower(os.Getenv("UPTRACE")) == "true" && os.Getenv("UPTRACE_DSN") != "" {
		uptrace.ConfigureOpentelemetry(
			uptrace.WithDSN(os.Getenv("UPTRACE_DSN")),
			uptrace.WithServiceName(os.Getenv("UPTRACE_SERVICE_NAME")),
			uptrace.WithServiceVersion("v1.0.0"),
		)

		_ = DB.Use(tracing.NewPlugin())
	}

	//get database
	if sql_, err = DB.DB(); err != nil {
		utils.ErrorHandler(err)
		panic(err)
	}

	if pgsql.Config.MAX_IDLE_CONNS_TIME > 0 {
		sql_.SetConnMaxIdleTime(time.Duration(pgsql.Config.MAX_IDLE_CONNS_TIME) * time.Minute)
	}
	if pgsql.Config.MAX_IDLE_CONNS_TIME > 0 {
		sql_.SetMaxIdleConns(pgsql.Config.MAX_IDLE_CONNS)
	}
	if pgsql.Config.MAX_OPEN_CONNS > 0 {
		sql_.SetMaxOpenConns(pgsql.Config.MAX_OPEN_CONNS)
	}

	return
}
