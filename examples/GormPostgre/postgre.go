package main

import (
	"gitlab.com/modules-shortcut/go-database-manager/databases/postgre"
	"gitlab.com/modules-shortcut/go-utils"
)

//Example model User

type User struct {
	//Must Extend with Model struct, so we can use all function from this struct to struct User
	postgre.Model

	Name string `json:"name" form:"name"`
	Pets []Pet  `json:"pets" gorm:"foreignKey:OwnerID"`
}

type Pet struct {
	postgre.Model
	OwnerID   uint    `json:"owner_id" form:"owner_id"`
	Name      string  `json:"name" form:"name"`
	Age       int     `json:"age" form:"age"`
	PetKindID uint    `json:"pet_kind_id" form:"pet_kind_id"`
	PetKind   PetKind `json:"pet_kind"`
}

type PetKind struct {
	postgre.Model
	Name string `json:"name" form:"name"`
}

func init() {
	db, sql := postgre.DB(false, nil)
	defer utils.CloseDB(sql)

	err := db.AutoMigrate(
		&PetKind{},
		&Pet{},
		&User{},
	)

	if err != nil {
		panic(err)
	}

}

func main() {

	var (
		responses []User
		model     User
		err       error
	)

	//Create single data
	user := User{
		Name: "Diana",
		Pets: []Pet{
			{
				Name: "Oscar",
				Age:  1,
				PetKind: PetKind{
					Name: "Lizard",
				},
			},
		},
	}
	if err = model.Create(&user); err != nil {
		//Do something to handle if you get error
	}

	//Create bulk data
	users := []User{
		{
			Name: "Anton",
			Pets: []Pet{
				{
					Name: "Blacky",
					Age:  3,
					PetKind: PetKind{
						Name: "Dog",
					},
				},
				{
					Name: "Lili",
					Age:  1,
					PetKind: PetKind{
						Name: "Bird",
					},
				},
			},
		},
	}
	if err = model.Create(&users); err != nil {
		//Do something to handle if you get error
	}

	// Get All users without relation
	if err = model.Read(&responses); err != nil {
		//Do something to handle if you get error
	}

	// Get All users with their pets
	if err = model.SetPreload("Pets").Read(&responses); err != nil {
		//Do something to handle if you get error
	}

	//Get User where name equal Anton
	if err = model.SetPreload("Pets").Read(&responses, "name = ?", "Anton"); err != nil {
		//Do something to handle if you get error
	}
}
